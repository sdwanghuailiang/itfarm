package com.tc.itfarm.admin.action.system;

import com.tc.itfarm.model.SystemConfig;
import com.tc.itfarm.service.SystemConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by wangdongdong on 2016/9/8.
 */
@Controller
@RequestMapping("/system")
public class SystemAction {

    @Resource
    private SystemConfigService systemConfigService;

    @RequestMapping("/index")
    public String index(Model model) {
        SystemConfig config = systemConfigService.selectAll().get(0);
        model.addAttribute("config", config);
        return "admin/system/index";
    }

    @RequestMapping("/save")
    public String save(SystemConfig config) {
        systemConfigService.updateBySelective(config);
        return "redirect:index.do";
    }
}
