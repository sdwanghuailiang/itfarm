package com.tc.itfarm.admin.action;

import com.tc.itfarm.model.Menu;
import com.tc.itfarm.model.SystemConfig;
import com.tc.itfarm.service.LogService;
import com.tc.itfarm.service.MenuService;
import com.tc.itfarm.service.SystemConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("admin")
public class AdminAction {

	@Resource
	private MenuService menuService;
	@Resource
	private LogService logService;
	
	@RequestMapping("index")
	public String index(Model model, HttpServletRequest request) {
		// 读取菜单和系统配置
		model.addAttribute("menus", menuService.selectAll());
		model.addAttribute("logs", logService.selectByType("登陆"));
		return "admin/index";
	}

	@RequestMapping("user/index")
	public String userAdmin() {
		return "admin/user/index";
	}

}
