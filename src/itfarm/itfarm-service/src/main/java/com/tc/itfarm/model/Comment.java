package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private Integer recordId;

    private String content;

    private String articleTitle;

    private Short type;

    private Integer parentId;

    private Integer userId;

    private String nickName;

    private Integer likeCount;

    private String email;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Comment(Integer recordId, String content, String articleTitle, Short type, Integer parentId, Integer userId, String nickName, Integer likeCount, String email, Date createTime) {
        this.recordId = recordId;
        this.content = content;
        this.articleTitle = articleTitle;
        this.type = type;
        this.parentId = parentId;
        this.userId = userId;
        this.nickName = nickName;
        this.likeCount = likeCount;
        this.email = email;
        this.createTime = createTime;
    }

    public Comment() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle == null ? null : articleTitle.trim();
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}