package com.tc.itfarm.api.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtils {

	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	public static final String DATE_FORMAT_MINITE = "yyyy-MM-dd HH:mm";

	public static final String DATE_FORMAT_SECONDE = "yyyy-MM-dd HH:mm:ss";

	public static final String DATE_FORMAT_CHINESE = "yyyy年MM月dd日";
	
	public static final String DATE_FORMAT_CHINESE_SECONDE = "yyyy年MM月dd日 HH:mm:ss";

	/**
	 * 日期转换为中文格式
	 * @param date
	 * @return
	 */
	public static String dateToChineseString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_CHINESE);
		return sdf.format(date);
	}
	
	public static Date now(){
		return new Date();
	}

	/**
	 * 字符串转日期
	 * @param strDate
	 * @return
	 */
	public static Date strToDate(String strDate) {
		if (StringUtils.isBlank(strDate)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_CHINESE);
		try {
			return sdf.parse(strDate);
		} catch (ParseException e) {
			logger.error("时间格式不正确!");
		}
		return null;
	}


}
